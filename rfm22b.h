/****************************************************************************************************************************************
This library contains a number of functions used to control the operation of a HOPERF RF22B module. It is used in conjuntion with the
BBB_spi.h library which handles the actual SPI communications. Originally created by Owen McAree 2013
****************************************************************************************************************************************/
///Libraries
#ifndef rfm22b_h
#define rfm22b_h

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include "BBB_spi.h"
#include "rfm22b_regs.h"

/****************************************************************************************************************************************/
///Function Prototypes

// Set or get the carrier frequency (in Hz);
void setCarrierFrequency(unsigned int frequency);
unsigned int getCarrierFrequency();
// Set or get the frequency hopping step size (in Hz, but it is floored to nearest 10kHz)
void setFrequencyHoppingStepSize(unsigned int step);
unsigned int getFrequencyHoppingStepSize();

// Set or get the frequency hopping channel
void setChannel(uint8_t channel);
uint8_t getChannel();

// Set or get the frequency deviation (in Hz, but floored to the nearest 625Hz)
void setFrequencyDeviation(unsigned int deviation);
unsigned int getFrequencyDeviation();

// Set or get the data rate (bps)
void setDataRate(unsigned int rate);
unsigned int getDataRate();

// Set or get the modulation type
void setModulationType(unsigned int modulation);
unsigned int getModulationType();

// Set or get the modulation data source
void setModulationDataSource(unsigned int source);
unsigned int getModulationDataSource();

// Set or get the data clock source
void setDataClockConfiguration( unsigned int clock);
unsigned int getDataClockConfiguration();

// Set or get the transmission power
void setTransmissionPower(uint8_t power);
uint8_t getTransmissionPower();

// Set or get the GPIO configuration
void setGPIOFunction(unsigned int gpio, unsigned int funct);
// This should probably return enum, but this needs a lot of cases
uint8_t getGPIOFunction(unsigned int gpio);

// Enable or disable interrupts
// No ability to get interrupt enable status as this would need a lot of case statements
void setInterruptEnable(unsigned int interrupt, unsigned int enable);

// Get the status of an interrupt
unsigned int getInterruptStatus(unsigned int interrupt);

// Set the operating mode
//        This function does not toggle individual pins as with other functions
//        It expects a bitwise-ORed combination of the modes you want set
void setOperatingMode(uint16_t mode);

// Get operating mode (bitwise-ORed)
uint16_t getOperatingMode();

// Manually enable RX or TX modes
void enableRXMode();
void enableTXMode();

// Reset the device
void reset();

// Set or get the trasmit header
void setTransmitHeader(uint32_t header);
uint32_t getTransmitHeader();

// Set or get the check header
void setCheckHeader(uint32_t header);
uint32_t getCheckHeader();

// Get and set all the FIFO threshold
void setTXFIFOAlmostFullThreshold(uint8_t thresh);
void setTXFIFOAlmostEmptyThreshold(uint8_t thresh);
void setRXFIFOAlmostFullThreshold(uint8_t thresh);
uint8_t getTXFIFOAlmostFullThreshold();
uint8_t getTXFIFOAlmostEmptyThreshold();
uint8_t getRXFIFOAlmostFullThreshold();

// Get RSSI value or input dBm
int16_t getRSSI();
int8_t getInputPower();

// Get length of last received packet
uint8_t getReceivedPacketLength();

// Set length of packet to be transmitted
void setTransmitPacketLength(uint8_t length);

// Clear the FIFOs
void clearRXFIFO();
void clearTXFIFO();

// Send data
void send(uint8_t *data, int length);

// Receive data (blocking with timeout). Returns number of bytes received
//     int timeout=30000;
//     int receive(uint8_t *data, int length, int timeout);

// Helper functions for getting and getting individual registers
uint8_t getRegister(uint8_t reg);
uint16_t get16BitRegister(uint8_t reg);
uint32_t get32BitRegister(uint8_t reg);
void setRegister(uint8_t reg, uint8_t value);
void set16BitRegister(uint8_t reg, uint16_t value);
void set32BitRegister(uint8_t reg, uint32_t value);

//static const uint8_t MAX_PACKET_LENGTH = 64;
#ifndef MAX_PACKET_LENGTH
#define MAX_PACKET_LENGTH 64
#endif

void setFIFOThreshold(uint8_t reg, uint8_t thresh);

void radio_test(); //Simple function which tests response of RF22B

/****************************************************************************************************************************************/

#endif
