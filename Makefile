CXXFLAGS=-Wall -lrt

all: main

main : main.c BBB_spi.o rfm22b.o
	gcc $(CXXFLAGS) main.c BBB_spi.o rfm22b.o -o main.exe

BBB_spi.o: BBB_spi.c BBB_spi.h

rfm22b.o: rfm22b.c rfm22b.h

clean:
	rm BBB_spi.o rfm22b.o BBB_gpio.o
