/****************************************************************************************************************************************
This is the main function of the BBB implant localisation control program.
Created by Matthew Magill
****************************************************************************************************************************************/
///Libraries
#include "BBB_spi.h" //Library which controls SPI functions of BBB
#include "rfm22b.h" //Library which handles RF22B funtionality
#include "BBB_gpio.c"
/****************************************************************************************************************************************/
///Function Prototypes
void setup_tx();
void setup_rx(int);
void cui_setup();
void rssi_display();
/****************************************************************************************************************************************/
///Global Variables
int receiver_num;
/****************************************************************************************************************************************/
///Main
int main(int argc, char **argv)
{
    SPI("/dev/spidev1.0"); //Sets up SPI communication through SPI Bus interface spidev1.0 found in the linux kernel

    GPIO_init();//Initialise GPIO pins

    GPIO_map();//Allows for memory mapped GPIO functionality
    GPIO_set();//Sets all GPIO pins high initially as required for SPI CS lines

   cui_setup();//Sets up Character User Interface and configures system as required by users requirements

   rssi_display();//Begins polling configured RFM22Bs RSSI registers and displaying them on terminal

    close(fd);//All open file pointers closed
    spiClose();



return(0);
}
/****************************************************************************************************************************************/
///Main
void rssi_display()//NOTE- Reintroduce kill_x functinality
{
    char kill_x;

    switch(receiver_num)
   {
    case 0 :
    while(1)
    {
    spi_device=0;
    printf("Transmitter Active");
    printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    printf("Enter x to exit\n");
    //scanf("%c", &kill_x);
    if (kill_x == 'x')
    {
        break;
    }
    sleep(1);
    }
    break;

    case 1 :
    do
    {
    spi_device=1;
    printf("Receiver 1 RSSI reading: %d dBm",getRSSI());
    printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    printf("Enter x to exit\n");
    //kill_x=getchar();
    sleep(1);
    }while(kill_x != 'x' );

    break;

    case 2 :
    while(1)
    {
    spi_device=1;
    printf("Receiver 1 RSSI reading: %f dBm\n",getRSSI());
    spi_device=2;
    printf("Receiver 2 RSSI reading: %f dBm",getRSSI());
    printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    printf("Enter x to exit\n");
    //scanf("%c", &kill_x);
    if (kill_x == 'x')
    {
        break;
    }
    sleep(1);
    }
    break;

    case 3 :
    while(1)
    {
    spi_device=1;
    printf("Receiver 1 RSSI reading: %f dBm\n",getRSSI());
    spi_device=2;
    printf("Receiver 2 RSSI reading: %f dBm\n",getRSSI());
    spi_device=3;
    printf("Receiver 3 RSSI reading: %f dBm",getRSSI());
    printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
    printf("Enter x to exit\n");
    //scanf("%c", &kill_x);
    if (kill_x == 'x')
    {
        break;
    }
    sleep(1);
    }
    break;
   }

}

void cui_setup()
{
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("////////////////////////Beaglebone Black Remote Command Terminal/////////////////////////\n");
    printf("//////////////////////////////////////version 1.0////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("/////////////////////////////////////////////////////////////////////////////////////////\n");
    printf("//////////////////////////////////Matthew Magill 2014////////////////////////////////////\n");
    sleep(2);

    printf("How many receivers are connected? (0,1,2 or 3)");
    printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n ");

    scanf("%d", &receiver_num);


   switch(receiver_num)
   {
    case 0 :
        setup_tx();
        break;
    case 1 :
        setup_tx();
        setup_rx(1);
        break;
    case 2 :
        setup_tx();
        setup_rx(1);
        setup_rx(2);
        break;
    case 3 :
        setup_tx();
        setup_rx(1);
        setup_rx(2);
        setup_rx(3);
        break;
   }

}
void setup_tx()//Sets up RFM22B in expansion PCB slot0 as transmitter
{
    spi_device=0;//spi_device 0 always transmitter
    setCarrierFrequency(434E6);//Carrier initally ISM band @ 434 MHz
    setModulationType(UNMODULATED_CARRIER);//Unmodulated Carrier
    setTransmissionPower(1);//Low transmission power to avoid overloading receiver front end
    setGPIOFunction(GPIO0, TX_STATE);//Configuring GPIO for TX/RX state switching
    setGPIOFunction(GPIO1, RX_STATE);
    setTransmitHeader(123456789);//Packet header

    clearTXFIFO();//Clearing TX FIFO
    enableTXMode();//Enabling TX mode
}

void setup_rx(board)//Function configures board as defined by int passed to function (1-5)
{
    spi_device=board;
    setCarrierFrequency(434E6);
    setModulationType(UNMODULATED_CARRIER);
    //setModulationDataSource(PN9);
    //setDataClockConfiguration(NONE);
    setGPIOFunction(GPIO0, TX_STATE);
    setGPIOFunction(GPIO1, RX_STATE);
    setCheckHeader(123456789);

    clearRXFIFO();
    enableRXMode();
}

/*
    spi_device=0;
    radio_test();
    spi_device=1;
    radio_test();
    spi_device=2;
    radio_test();
    spi_device=3;
    radio_test();
*/
