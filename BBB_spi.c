#include "BBB_spi.h"

//        Default bits per word is 8
//        Default clock speed is 10kHz
SPI(const char *device) {
    device = device;
    fp = open(device, O_RDWR);
        if (fp < 0) {
        printf("Unable to open SPI device\n");
        }
        else{
        setMode(0);
        setBitsPerWord(8);
        setMaxSpeedHz(1000000);
        setDelayUsecs(0);
        printf("Successfully opened spi device\n");
        }
}

// Set the mode of the bus (see linux/spi/spidev.h)
void setMode(uint8_t mode) {
    int ret = ioctl(fp, SPI_IOC_WR_MODE, &mode);
    if (ret == -1)
        printf("Unable to set SPI mode");

    ret = ioctl(fp, SPI_IOC_RD_MODE, &mode);
    if (ret == -1)
        printf("Unable to get SPI mode");
}

// Get the mode of the bus
uint8_t getMode() {
    return mode;
}

// Set the number of bits per word
void setBitsPerWord(uint8_t bits) {
    int ret = ioctl(fp, SPI_IOC_WR_BITS_PER_WORD, &bits);
    if (ret == -1)
        printf("Unable to set bits per word");

    ret = ioctl(fp, SPI_IOC_RD_BITS_PER_WORD, &bits);
    if (ret == -1)
        printf("Unable to get bits per word");
}

// Get the number of bits per word
uint8_t getBitsPerWord() {
    return bits;
}

// Set the bus clock speed
void setMaxSpeedHz(uint32_t speed) {
    int ret = ioctl(fp, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
    if (ret == -1)
        printf("Unable to set max speed Hz");

    ret = ioctl(fp, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
    if (ret == -1)
        printf("Unable to get max speed Hz");
}

// Get the bus clock speed
uint32_t getMaxSpeedHz() {
    return speed;
}

// Set the bus delay
void setDelayUsecs(uint16_t delay) {
    delay = delay;
}

// Get the bus delay
uint16_t getDelayUsecs() {
    return delay;
}

// Transfer some data
int transfer(uint8_t *tx, uint8_t *rx, int length) {
    struct spi_ioc_transfer tr;

    tr.tx_buf = (unsigned long)tx;        //tx and rx MUST be the same length!
    tr.rx_buf = (unsigned long)rx;
    tr.len = length;
    tr.delay_usecs = delay;
    tr.speed_hz = speed;
    tr.cs_change=0;
    tr.bits_per_word = bits;

    int ret = ioctl(fp, SPI_IOC_MESSAGE(1), &tr);
    if (ret == 1) {
       printf("Unable to send spi message");
         return 0;
         }

         return 1;

}

// Close the bus
void spiClose() {
   close(fp);
   printf("SPI bus closed\n");
}
