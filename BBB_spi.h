/****************************************************************************************************************************************
This library contains a number of functions to allow SPI serial communication btween a Beaglebone Black and a HOPERF RF22B radio module.
GND P9_1
VCC P9_3
SDO P9_21
SDI P9_18
SCK P9_22
NSEL P9_17 (spidev1.0 default)


TX_ANT -> GPIO_0
RX_ANT -> GPIO_0
SDN -> GND (Already connected if using Sparkfun breakout boards)
****************************************************************************************************************************************/
///Libraries
#ifndef BBB_spi_h
#define BBB_spi_h

#include "rfm22b.h"

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>
/****************************************************************************************************************************************/
///Function Prototypes
SPI(const char *device);                                //Opens up kernel module for SPI communications

void setMode(uint8_t mode);                             //Set or get the SPI mode
uint8_t getMode();

void setBitsPerWord(uint8_t bits);                      //Set or get bits per word
uint8_t getBitsPerWord();

void setMaxSpeedHz(uint32_t speed);                     // Set or get the SPI clock speed
uint32_t getMaxSpeedHz();

void setDelayUsecs(uint16_t delay);                     // Set or get the SPI delay
uint16_t getDelayUsecs();

int transfer(uint8_t *tx, uint8_t *rx, int length);     //When sending data, both tx and rx arrays of the same length must be passed, in half duplex mode the unused array can be ignored however

void spiClose();                                        //Closes the SPI bus
/****************************************************************************************************************************************/
///Global Variables
const char *device;
int fp;
uint8_t mode;
uint8_t bits;
uint32_t speed;
uint16_t delay;
/****************************************************************************************************************************************/

#endif
