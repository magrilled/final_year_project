#include "BBB_gpio.h"

void GPIO_init()
{
    char set_value[4];

	if ((fp1 = fopen("/sys/class/gpio/export", "ab")) == NULL)//Opens up file pointer to export various GPIO pins
		{
			printf("Cannot open export file.\n");
			exit(1);
		}
		rewind(fp1);
		strcpy(set_value,"60");
		fwrite(&set_value, sizeof(char), 2, fp1);
        rewind(fp1);
		strcpy(set_value,"50");
		fwrite(&set_value, sizeof(char), 2, fp1);
        rewind(fp1);
		strcpy(set_value,"48");
		fwrite(&set_value, sizeof(char), 2, fp1);
		rewind(fp1);
		strcpy(set_value,"51");
		fwrite(&set_value, sizeof(char), 2, fp1);
		fclose(fp1);

	printf("New GPIO pins now accessible\n");

	if ((fp1 = fopen("/sys/class/gpio/gpio60/direction", "rb+")) == NULL)//Sets all GPIO pins as outputs
	{
		printf("Cannot open direction file.\n");
		exit(1);
	}

	rewind(fp1);
	strcpy(set_value,"out");
	fwrite(&set_value, sizeof(char), 3, fp1);
	fclose(fp1);
	printf("GPIO60 direction set to output\n");

    if ((fp1 = fopen("/sys/class/gpio/gpio50/direction", "rb+")) == NULL)
	{
		printf("Cannot open direction file.\n");
		exit(1);
	}
	rewind(fp1);
	strcpy(set_value,"out");
	fwrite(&set_value, sizeof(char), 3, fp1);
	fclose(fp1);
	printf("GPIO50 direction set to output\n");

    if ((fp1 = fopen("/sys/class/gpio/gpio48/direction", "rb+")) == NULL)
	{
		printf("Cannot open direction file.\n");
		exit(1);
	}

	rewind(fp1);
	strcpy(set_value,"out");
	fwrite(&set_value, sizeof(char), 3, fp1);
	fclose(fp1);
	printf("GPIO48 direction set to output\n");

    if ((fp1 = fopen("/sys/class/gpio/gpio51/direction", "rb+")) == NULL)
	{
		printf("Cannot open direction file.\n");
		exit(1);
	}

	rewind(fp1);
	strcpy(set_value,"out");
	fwrite(&set_value, sizeof(char), 3, fp1);
	fclose(fp1);
	printf("GPIO51 direction set to output\n");





	if ((fpTx = fopen("/sys/class/gpio/gpio60/value", "rb+")) == NULL)//Sets all GPIO pins to be high initaially as required by SPI CS lines
		{
			printf("Cannot open value file for GPIO60.\n");
			exit(1);
		}
    else
        {
            rewind(fpTx);
            strcpy(set_value,"1");
            fwrite(&set_value, sizeof(char), 3, fpTx);
            printf("GPIO60 sucessfully initialised\n");
        }

	if ((fpRx1 = fopen("/sys/class/gpio/gpio50/value", "rb+")) == NULL)
		{
			printf("Cannot open value file for GPIO50.\n");
			exit(1);
		}
    else
        {
            rewind(fpRx1);
            strcpy(set_value,"1");
            fwrite(&set_value, sizeof(char), 3, fpRx1);
            printf("GPIO50 sucessfully initialised\n");
        }

	if ((fpRx2 = fopen("/sys/class/gpio/gpio48/value", "rb+")) == NULL)
		{
			printf("Cannot open value file for GPIO48.\n");
			exit(1);
		}
    else
        {
            rewind(fpRx2);
            strcpy(set_value,"1");
            fwrite(&set_value, sizeof(char), 3, fpRx2);
            printf("GPIO48 sucessfully initialised\n");
        }

	if ((fpRx3 = fopen("/sys/class/gpio/gpio51/value", "rb+")) == NULL)
		{
			printf("Cannot open value file for GPIO51.\n");
			exit(1);
		}
    else
        {
            rewind(fpRx3);
            strcpy(set_value,"1");
            fwrite(&set_value, sizeof(char), 3, fpRx3);
            printf("GPIO51 sucessfully initialised\n");
        }
    fclose(fpTx);
    fclose(fpRx1);
    fclose(fpRx2);
    fclose(fpRx3);

}

void GPIO_map()
{
    fd = open("/dev/mem", O_RDWR);

    gpio_addr = mmap(0, GPIO1_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, GPIO1_START_ADDR);

    gpio_oe_addr = gpio_addr + GPIO_OE;
    gpio_setdataout_addr = gpio_addr + GPIO_SETDATAOUT;
    gpio_cleardataout_addr = gpio_addr + GPIO_CLEARDATAOUT;

    if(gpio_addr == MAP_FAILED) {
        printf("Unable to map GPIO\n");
        exit(1);
    }
    printf("GPIO1 successfully mapped\n");



}

void GPIO_set()//Sets GPIO pin described by global variable spi_device high **Global variable dependance to be removed**
{
    switch(spi_device)
    {
    case 0 :
        reg = *gpio_oe_addr;
        reg = reg & (0xFFFFFFFF - GPIO60);
        *gpio_oe_addr = reg;
        *gpio_setdataout_addr= GPIO60;
        break;
    case 1 :
        reg = *gpio_oe_addr;
        reg = reg & (0xFFFFFFFF - GPIO50);
        *gpio_oe_addr = reg;
        *gpio_setdataout_addr= GPIO50;
        break;
    case 2 :
        reg = *gpio_oe_addr;
        reg = reg & (0xFFFFFFFF - GPIO48);
        *gpio_oe_addr = reg;
        *gpio_setdataout_addr= GPIO48;
        break;
    case 3 :
        reg = *gpio_oe_addr;
        reg = reg & (0xFFFFFFFF - GPIO51);
        *gpio_oe_addr = reg;
        *gpio_setdataout_addr= GPIO51;
        break;

    }
}

void GPIO_clear()
{
    switch(spi_device)
    {
    case 0 :
        reg = *gpio_oe_addr;
        reg = reg & (0xFFFFFFFF - GPIO60);
        *gpio_oe_addr = reg;
        *gpio_cleardataout_addr= GPIO60;
        break;
    case 1 :
        reg = *gpio_oe_addr;
        reg = reg & (0xFFFFFFFF - GPIO50);
        *gpio_oe_addr = reg;
        *gpio_cleardataout_addr= GPIO50;
        break;
    case 2 :
        reg = *gpio_oe_addr;
        reg = reg & (0xFFFFFFFF - GPIO48);
        *gpio_oe_addr = reg;
        *gpio_cleardataout_addr= GPIO48;
        break;
    case 3 :
        reg = *gpio_oe_addr;
        reg = reg & (0xFFFFFFFF - GPIO51);
        *gpio_oe_addr = reg;
        *gpio_cleardataout_addr= GPIO51;
        break;

    }
}




