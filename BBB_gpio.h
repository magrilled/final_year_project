/****************************************************************************************************************************************
This library contains a number of functions to the mapping and manipulation of various predetermined GPIO pins on the BBB
****************************************************************************************************************************************/
///Libraries
#ifndef BBB_gpio_h
#define BBB_gpio_h

#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
/****************************************************************************************************************************************/
///Defined Constants
#define GPIO1_START_ADDR 0x4804C000//Start and end addresses define memory addresses relating to GPIO1 chip
#define GPIO1_END_ADDR 0x4804DFFF
#define GPIO1_SIZE (GPIO1_END_ADDR - GPIO1_START_ADDR)
#define GPIO_OE 0x134
#define GPIO_SETDATAOUT 0x194//Set and clear values for GPIO pins
#define GPIO_CLEARDATAOUT 0x190

#define USR0_LED (1<<21)//Bit offsets relating to various GPIO Pins
#define USR1_LED (1<<22)
#define USR2_LED (1<<23)
#define USR3_LED (1<<24)
#define GPIO60 (1<<28)
#define GPIO50 (1<<18)
#define GPIO48 (1<<16)
#define GPIO51 (1<<19)
/****************************************************************************************************************************************/
///Function Prototypes
    void GPIO_init();//Exports and initalises GPIO pins using sysfs calls
    void GPIO_map();//Maps entire memory address range relating to GPIO1 chip
    void GPIO_set();//Sets CS pin relating to current spi_device high
    void GPIO_clear();//Sets CS pin relating to current spi_device high
/****************************************************************************************************************************************/
///Global Variables
    volatile void *gpio_addr = NULL;
    volatile unsigned int *gpio_oe_addr = NULL;
    volatile unsigned int *gpio_setdataout_addr = NULL;
    volatile unsigned int *gpio_cleardataout_addr = NULL;
    unsigned int reg;
    int fd;
    FILE *fp1,*fpTx,*fpRx1,*fpRx2,*fpRx3;

    int spi_device; //Variable used to describe which SPI device is have access to the bus. Used by the transfer function to hold the chip select line high 0=Tx 1=Rx1 2=Rx2 3=Rx3

#endif
