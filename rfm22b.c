#include "rfm22b.h"
//#include "BBB_gpio.h"

// Set the frequency of the carrier wave
//        This function calculates the values of the registers 0x75-0x77 to achieve the
//        desired carrier wave frequency (without any hopping set)
//        Frequency should be passed in integer Hertz
void setCarrierFrequency(unsigned int frequency) {
        // Don't set a frequency outside the range specified in the datasheet
        if (frequency < 240E6 || frequency > 960E6) {
                printf("Cannot set carrier frequency to %fMHz, out of range!",frequency/1E6f);
                return;
        }

        // The following determines the register values, see Section 3.5.1 of the datasheet

        // Are we in the 'High Band'? (i.e. is hbsel == 1)
        uint8_t hbsel = (frequency >= 480E6);

        // What is the integer part of the frequency
        uint8_t fb = frequency/10E6/(hbsel+1) - 24;

        // Calculate register 0x75 from hbsel and fb. sbsel (bit 6) is always set
        uint8_t fbs = (1<<6) | (hbsel<<5) | fb;

        // Calculate the fractional part of the frequency
        uint16_t fc = (frequency/(10E6f*(hbsel+1)) - fb - 24) * 64000;

        // Split the fractional part in to most and least significant bits
        // (Registers 0x76 and 0x77 respectively)
        uint8_t ncf1 = (fc >> 8);
        uint8_t ncf0 = fc & 0xff;

        // Write the registers to the device
        setRegister(FREQUENCY_BAND_SELECT, fbs);
        setRegister(NOMINAL_CARRIER_FREQUENCY_1, ncf1);
        setRegister(NOMINAL_CARRIER_FREQUENCY_0, ncf0);
}

// Get the frequency of the carrier wave in integer Hertz
//        Without any frequency hopping
unsigned int getCarrierFrequency() {
        // Read the register values
        uint8_t fbs = getRegister(FREQUENCY_BAND_SELECT);
        uint8_t ncf1 = getRegister(NOMINAL_CARRIER_FREQUENCY_1);
        uint8_t ncf0 = getRegister(NOMINAL_CARRIER_FREQUENCY_0);

        // The following calculations ceom from Section 3.5.1 of the datasheet

        // Determine the integer part
        uint8_t fb = fbs & 0x1F;

        // Are we in the 'High Band'?
        uint8_t hbsel = (fbs >> 5) & 1;

        // Determine the fractional part
        uint16_t fc = (ncf1 << 8) | ncf0;

        // Return the frequency
        return 10E6*(hbsel+1)*(fb+24+fc/64000.0);
}

// Get and set the frequency hopping step size
//        Values are in Hertz (to stay SI) but floored to the nearest 10kHz
void setFrequencyHoppingStepSize(unsigned int step) {
        if (step > 2550000) {
                step = 2550000;
        }
        setRegister(FREQUENCY_HOPPING_STEP_SIZE, step/10000);
}
unsigned int getFrequencyHoppingStepSize() {
        unsigned int temp = getRegister(FREQUENCY_HOPPING_STEP_SIZE);
        return temp*10000;
}

// Get and set the frequency hopping channel
void setChannel(uint8_t channel) {
        setRegister(FREQUENCY_HOPPING_CHANNEL_SELECT, channel);
}
uint8_t getChannel() {
        return getRegister(FREQUENCY_HOPPING_CHANNEL_SELECT);
}

// Set or get the frequency deviation (in Hz, but floored to the nearest 625Hz)
void setFrequencyDeviation(unsigned int deviation) {
        if (deviation > 320000) {
                deviation = 320000;
        }
        setRegister(FREQUENCY_DEVIATION, deviation/625);
}
unsigned int getFrequencyDeviation() {
        return getRegister(FREQUENCY_DEVIATION)*625;
}

// Set or get the data rate (bps)
void setDataRate(unsigned int rate) {
        // Get the Modulation Mode Control 1 register (for scaling bit)
        uint8_t mmc1 = getRegister(MODULATION_MODE_CONTROL_1);

        uint16_t txdr;
        // Set the scale bit (5th bit of 0x70) high if data rate is below 30kbps
        // and calculate the value for txdr registers (0x6E and 0x6F)
        if (rate < 30000) {
                mmc1 |= (1<<5);
                txdr = rate * ((1 << (16 + 5)) / 1E6);
        } else {
                mmc1 &= ~(1<<5);
                txdr = rate * ((1 << (16)) / 1E6);
        }

        // Set the data rate bytes
        set16BitRegister(TX_DATA_RATE_1, txdr);

        // Set the scaling byte
        setRegister(MODULATION_MODE_CONTROL_1, mmc1);
}
unsigned int getDataRate() {
        // Get the data rate scaling value (5th bit of 0x70)
        uint8_t txdtrtscale = (getRegister(MODULATION_MODE_CONTROL_1) >> 5) & 1;

        // Get the data rate registers
        uint8_t txdr = get16BitRegister(TX_DATA_RATE_1);

        // Return the data rate (in bps, hence extra 1E3)
        return (txdr * 1E6) / (1 << (16 + 5 * txdtrtscale));

}

// Set or get the modulation type
void setModulationType(unsigned int modulation) {
        // Get the Modulation Mode Control 2 register
        uint8_t mmc2 = getRegister(MODULATION_MODE_CONTROL_2);

        // Clear the modtyp bits
        mmc2 &= ~0x03;

        // Set the desired modulation
        mmc2 |= modulation;

        // Set the register
        setRegister(MODULATION_MODE_CONTROL_2, mmc2);
}
    unsigned int getModulationType() {
        // Get the Modulation Mode Control 2 register
        uint8_t mmc2 = getRegister(MODULATION_MODE_CONTROL_2);

        // Determine modtyp bits
        uint8_t modtyp = mmc2 & 0x03;

        // Ugly way to return correct enum
        switch (modtyp) {
                case 1:
                        return OOK;
                case 2:
                        return FSK;
                case 3:
                        return GFSK;
                case 0:
                default:
                        return UNMODULATED_CARRIER;
        }
}

void setModulationDataSource(unsigned int source) {
        // Get the Modulation Mode Control 2 register
        uint8_t mmc2 = getRegister(MODULATION_MODE_CONTROL_2);

        // Clear the dtmod bits
        mmc2 &= ~(0x03<<4);

        // Set the desired data source
        mmc2 |= source << 4;

        // Set the register
        setRegister(MODULATION_MODE_CONTROL_2, mmc2);
}
    unsigned int getModulationDataSource() {
        // Get the Modulation Mode Control 2 register
        uint8_t mmc2 = getRegister(MODULATION_MODE_CONTROL_2);

        // Determine modtyp bits
        uint8_t dtmod = (mmc2 >> 4) & 0x03;

        // Ugly way to return correct enum
        switch (dtmod) {
                case 1:
                        return DIRECT_SDI;
                case 2:
                        return FIFO;
                case 3:
                        return PN9;
                case 0:
                default:
                        return DIRECT_GPIO;
        }
}

void setDataClockConfiguration(unsigned int clock) {
        // Get the Modulation Mode Control 2 register
        uint8_t mmc2 = getRegister(MODULATION_MODE_CONTROL_2);

        // Clear the trclk bits
        mmc2 &= ~(0x03<<6);

        // Set the desired data source
        mmc2 |= clock << 6;

        // Set the register
        setRegister(MODULATION_MODE_CONTROL_2, mmc2);
}
    unsigned int getDataClockConfiguration() {
        // Get the Modulation Mode Control 2 register
        uint8_t mmc2 =getRegister(MODULATION_MODE_CONTROL_2);

        // Determine modtyp bits
        uint8_t dtmod = (mmc2 >> 6) & 0x03;

        // Ugly way to return correct enum
        switch (dtmod) {
                case 1:
                        return GPIO;
                case 2:
                        return SDO;
                case 3:
                        return NIRQ;
                case 0:
                default:
                        return NONE;
        }
}

// Set or get the transmission power
void setTransmissionPower(uint8_t power) {
        // Saturate to maximum power
        if (power > 20) {
                power = 20;
        }

        // Get the TX power register
        uint8_t txp = getRegister(TX_POWER);

        // Clear txpow bits
        txp &= ~(0x07);

        // Calculate txpow bits (See Section 5.7.1 of datasheet)
        uint8_t txpow = (power + 1) / 3;

        // Set txpow bits
        txp |= txpow;

        // Set the register
        setRegister(TX_POWER, txp);
}
uint8_t getTransmissionPower() {
        // Get the TX power register
        uint8_t txp = getRegister(TX_POWER);

        // Get the txpow bits
        uint8_t txpow = txp & 0x07;

        // Calculate power (see Section 5.7.1 of datasheet)
        if (txpow == 0) {
                return 1;
        } else {
                return txpow * 3 - 1;
        }
}

// Set or get the GPIO configuration
void setGPIOFunction(unsigned int gpio, unsigned int func) {
        // Get the GPIO register
        uint8_t gpioX = getRegister(gpio);

        // Clear gpioX bits
        gpioX &= ~((1<<5)-1);

        // Set the gpioX bits
        gpioX |= func;

        // Set the register
        setRegister(gpio, gpioX);
}

uint8_t getGPIOFunction(unsigned int gpio) {
        // Get the GPIO register
        uint8_t gpioX = getRegister(gpio);

        // Return the gpioX bits
        // This should probably return an enum, but this needs a lot of cases
        return gpioX & ((1<<5)-1);
}

// Enable or disable interrupts
void setInterruptEnable(unsigned int interrupt, unsigned int enable) {
        // Get the (16 bit) register value
        uint16_t intEnable = get16BitRegister(INTERRUPT_ENABLE_1);

        // Either enable or disable the interrupt
        if (enable) {
                intEnable |= interrupt;
        } else {
                intEnable &= ~interrupt;
        }

        // Set the (16 bit) register value
        set16BitRegister(INTERRUPT_ENABLE_1, intEnable);
}

// Get the status of an interrupt
unsigned int getInterruptStatus(unsigned int interrupt) {
        // Get the (16 bit) register value
        uint16_t intStatus = get16BitRegister(INTERRUPT_STATUS_1);

        // Determine if interrupt bit is set and return
        if ((intStatus & interrupt) > 0) {
                return 1;
        } else {
                return 0;
        }
}

// Set the operating mode
//        This function does not toggle individual pins as with other functions
//        It expects a bitwise-ORed combination of the modes you want set
void setOperatingMode(uint16_t mode) {
        set16BitRegister(OPERATING_MODE_AND_FUNCTION_CONTROL_1, mode);
}

// Get operating mode (bitwise-ORed)
uint16_t getOperatingMode() {
        return get16BitRegister(OPERATING_MODE_AND_FUNCTION_CONTROL_1);
}

// Manuall enter RX or TX mode
void enableRXMode() {
        setOperatingMode(READY_MODE | RX_MODE);
}
void enableTXMode() {
        setOperatingMode(READY_MODE | TX_MODE );
}

// Reset the device
void reset() {
        setOperatingMode(READY_MODE | RESET);
}

// Set or get the trasmit header
void setTransmitHeader(uint32_t header) {
        set32BitRegister(TRANSMIT_HEADER_3, header);
}
uint32_t getTransmitHeader() {
        return get32BitRegister(TRANSMIT_HEADER_3);
}

// Set or get the check header
void setCheckHeader(uint32_t header) {
        set32BitRegister(CHECK_HEADER_3, header);
}
uint32_t getCheckHeader() {
        return get32BitRegister(CHECK_HEADER_3);
}

// Get and set all the FIFO threshold
void setTXFIFOAlmostFullThreshold(uint8_t thresh) {
        setFIFOThreshold(TX_FIFO_CONTROL_1, thresh);
}
void setTXFIFOAlmostEmptyThreshold(uint8_t thresh) {
        setFIFOThreshold(TX_FIFO_CONTROL_2, thresh);
}
void setRXFIFOAlmostFullThreshold(uint8_t thresh) {
        setFIFOThreshold(RX_FIFO_CONTROL, thresh);
}
uint8_t getTXFIFOAlmostFullThreshold() {
        return getRegister(TX_FIFO_CONTROL_1);
}
uint8_t getTXFIFOAlmostEmptyThreshold() {
        return getRegister(TX_FIFO_CONTROL_2);
}
uint8_t getRXFIFOAlmostFullThreshold() {
        return getRegister(RX_FIFO_CONTROL);
}
void setFIFOThreshold(uint8_t reg, uint8_t thresh) {
        thresh &= ((1 << 6) - 1);
        setRegister(reg, thresh);
}

// Get RSSI value
int16_t getRSSI() {
        int8_t rssi_raw=getRegister(RECEIVED_SIGNAL_STRENGTH_INDICATOR);
        int16_t rssi_dbm;
        if (rssi_raw >= 127)
        {
            rssi_dbm = ( (rssi_raw - 256) / 2);
        }
        else
        {
            rssi_dbm = (rssi_raw / 2);
        }
        return rssi_dbm;
        //return rssi_raw;
}
// Get input power (in dBm)
//        Coefficients approximated from the graph in Section 8.10 of the datasheet
int8_t getInputPower() {
        return 0.56*getRSSI()-128.8;
}

// Get length of last received packet
uint8_t getReceivedPacketLength() {
        return getRegister(RECEIVED_PACKET_LENGTH);
}

// Set length of packet to be transmitted
void setTransmitPacketLength(uint8_t length) {
        return setRegister(TRANSMIT_PACKET_LENGTH, length);
}

void clearRXFIFO() {
        //Toggle ffclrrx bit high and low to clear RX FIFO
        setRegister(OPERATING_MODE_AND_FUNCTION_CONTROL_2, 2);
        setRegister(OPERATING_MODE_AND_FUNCTION_CONTROL_2, 0);
}

void clearTXFIFO() {
        //Toggle ffclrtx bit high and low to clear TX FIFO
        setRegister(OPERATING_MODE_AND_FUNCTION_CONTROL_2, 1);
        setRegister(OPERATING_MODE_AND_FUNCTION_CONTROL_2, 0);
}

// Send data
void send(uint8_t *data, int length) {
        // Clear TX FIFO
        clearTXFIFO();

        // Initialise rx and tx arrays
        uint8_t tx[MAX_PACKET_LENGTH+1];
        uint8_t rx[MAX_PACKET_LENGTH+1];

        // Set FIFO register address (with write flag)
        tx[0] = FIFO_ACCESS | (1<<7);

        // Truncate data if its too long
        if (length > MAX_PACKET_LENGTH) {
                length = MAX_PACKET_LENGTH;
        }

        // Copy data from input array to tx array
        int i=0;
        for (i = 1; i <= length; i++) {
                tx[i] = data[i-1];
        }

        // Set the packet length
        setTransmitPacketLength(length);

        // Make the transfer
        transfer(tx,rx,length+1);

        // Enter TX mode
        enableTXMode();

        // Loop until packet has been sent (device has left TX mode)
        while (((getRegister(OPERATING_MODE_AND_FUNCTION_CONTROL_1)>>3) & 1)) {}

        return;
};

// Receive data (blocking with timeout). Returns number of bytes received
int receive(uint8_t *data, int length, int timeout) {
        // Make sure RX FIFO is empty, ready for new data
        clearRXFIFO();

        // Enter RX mode
        enableRXMode();

        // Initialise rx and tx arrays
        uint8_t tx[MAX_PACKET_LENGTH+1];
        uint8_t rx[MAX_PACKET_LENGTH+1];

        // Set FIFO register address
        tx[0] = FIFO_ACCESS;

        // Timing for the interrupt loop timeout
        struct timeval start, end;
    gettimeofday(&start, NULL);
        long elapsed = 0;

        // Loop endlessly on interrupt or timeout
        //        Don't use interrupt registers here as these don't seem to behave consistently
        //        Watch the operating mode register for the device leaving RX mode. This is indicitive
        //        of a valid packet being received
        while (((getRegister(OPERATING_MODE_AND_FUNCTION_CONTROL_1)>>2) & 1) && elapsed < timeout) {
                // Determine elapsed time
                gettimeofday(&end, NULL);
                elapsed = (end.tv_usec - start.tv_usec)/1000 + (end.tv_sec - start.tv_sec)*1000;
        }

        // If timeout occured, return -1
        if (elapsed >= timeout) {
                printf("Rx Timeout\n");
                return -1;
        }

        // Get length of packet received
        uint8_t rxLength = getReceivedPacketLength();

        // Make the transfer
        transfer(tx,rx,rxLength+1);

        // Copy the data to the output array
        int i=0;
        for (i = 1; i <= rxLength; i++) {
                data[i-1] = rx[i];
        }

        return rxLength;
};

// Helper function to read a single byte from the device
uint8_t getRegister(uint8_t reg) {
        // rx and tx arrays must be the same length
        // Must be 2 elements as the device only responds whilst it is being sent
        // data. tx[0] should be set to the requested register value and tx[1] left
        // clear. Once complete, rx[0] will be left clear (no data was returned whilst
        // the requested register was being sent), and rx[1] will contain the value

            uint8_t tx[] = {0x00, 0x00};
            uint8_t rx[] = {0x00, 0x00};
            tx[0] = reg;

            GPIO_clear();
            transfer(tx,rx,2);
            GPIO_set();


            return rx[1];
}

// Similar to function above, but for readying 2 consequtive registers as one
uint16_t get16BitRegister(uint8_t reg) {
        uint8_t tx[] = {0x00, 0x00, 0x00};
        uint8_t rx[] = {0x00, 0x00, 0x00};

        tx[0] = reg;

        GPIO_clear();
        transfer(tx,rx,3);
        GPIO_set();

        return (rx[1] << 8) | rx[2];
}

// Similar to function above, but for readying 4 consequtive registers as one
uint32_t get32BitRegister(uint8_t reg) {
        uint8_t tx[] = {0x00, 0x00, 0x00, 0x00, 0x00};
        uint8_t rx[] = {0x00, 0x00, 0x00, 0x00, 0x00};

        tx[0] = reg;

        GPIO_clear();
        transfer(tx,rx,5);
        GPIO_set();

        return (rx[1] << 24) | (rx[2] << 16) | (rx[3] << 8) | rx[4];
}

// Helper function to write a single byte to a register
void setRegister(uint8_t reg, uint8_t value) {
        // tx and rx arrays required even though we aren't receiving anything
        uint8_t tx[] = {0x00, 0x00};
        uint8_t rx[] = {0x00, 0x00};

        // tx[0] is the requested register with the final bit set high to indicate
        // a write operation (see Section 3.1 of the datasheet)
        tx[0] = reg | (1<<7);

        // tx[1] is the value to be set
        tx[1] = value;

        GPIO_clear();
        transfer(tx,rx,2);
        GPIO_set();
}

// As above, but for 2 consequitive registers
void set16BitRegister(uint8_t reg, uint16_t value) {
        // tx and rx arrays required even though we aren't receiving anything
        uint8_t tx[] = {0x00, 0x00, 0x00};
        uint8_t rx[] = {0x00, 0x00, 0x00};

        // tx[0] is the requested register with the final bit set high to indicate
        // a write operation (see Section 3.1 of the datasheet)
        tx[0] = reg | (1<<7);

        // tx[1-2] is the value to be set
        tx[1] = (value >> 8);
        tx[2] = (value) & 0xFF;

        GPIO_clear();
        transfer(tx,rx,3);
        GPIO_set();
}

// As above, but for 4 consequitive registers
void set32BitRegister(uint8_t reg, uint32_t value) {
        // tx and rx arrays required even though we aren't receiving anything
        uint8_t tx[] = {0x00, 0x00, 0x00, 0x00, 0x00};
        uint8_t rx[] = {0x00, 0x00, 0x00, 0x00, 0x00};

        // tx[0] is the requested register with the final bit set high to indicate
        // a write operation (see Section 3.1 of the datasheet)
        tx[0] = reg | (1<<7);

        // tx[1-4] is the value to be set
        tx[1] = (value >> 24);
        tx[2] = (value >> 16) & 0xFF;
        tx[3] = (value >> 8) & 0xFF;
        tx[4] = (value) & 0xFF;

        GPIO_clear();
        transfer(tx,rx,5);
        GPIO_set();
}

void radio_test() {
        printf("Initial Settings...\n");
        printf("\tFrequency is %.3fMHz\n",getCarrierFrequency()/1E6f);
        printf("\tFH Step is %.3fkHz\n",getFrequencyHoppingStepSize()/1E3f);
        printf("\tChannel is %d\n",getChannel());
        printf("\tFrequency deviation is %.3fkHz\n",getFrequencyDeviation()/1E3f);
        printf("\tData rate is %.3fkbps\n",getDataRate()/1E3f);
        printf("\tModulation Type %d\n",getModulationType());
        printf("\tModulation Data Source %d\n",getModulationDataSource());
        printf("\tData Clock Configuration %d\n",getDataClockConfiguration());
        printf("\tTransmission Power is %ddBm\n",getTransmissionPower());
        printf("\tGPIO0 Function is 0x%.2X\n",getGPIOFunction(GPIO0));
        printf("\tGPIO1 Function is 0x%.2X\n",getGPIOFunction(GPIO1));
        printf("\tGPIO2 Function is 0x%.2X\n",getGPIOFunction(GPIO2));

        setCarrierFrequency(869E6);
        setFrequencyHoppingStepSize(20000);
        setChannel(3);
        setFrequencyDeviation(62500);
        setDataRate(15000);
        setModulationType(GFSK);
        setModulationDataSource(FIFO);
        setDataClockConfiguration(NONE);
        setTransmissionPower(11);
        setGPIOFunction(GPIO0, TX_STATE);
        setGPIOFunction(GPIO1, RX_STATE);
        setGPIOFunction(GPIO2, CLEAR_CHANNEL_ASSESSMENT);

        printf("\nNew Settings...\n");
        printf("\tFrequency is %.3fMHz\n",getCarrierFrequency()/1E6f);
        printf("\tFH Step is %.3fkHz\n",getFrequencyHoppingStepSize()/1E3f);
        printf("\tChannel is %d\n",getChannel());
        printf("\tFrequency deviation is %.3fkHz\n",getFrequencyDeviation()/1E3f);
        printf("\tData rate is %.3fkbps\n",getDataRate()/1E3f);
        printf("\tModulation Type %d\n",getModulationType());
        printf("\tModulation Data Source %d\n",getModulationDataSource());
        printf("\tData Clock Configuration %d\n",getDataClockConfiguration());
        printf("\tTransmission Power is %ddBm\n",getTransmissionPower());
        printf("\tGPIO0 Function is 0x%.2X\n",getGPIOFunction(GPIO0));
        printf("\tGPIO1 Function is 0x%.2X\n",getGPIOFunction(GPIO1));
        printf("\tGPIO2 Function is 0x%.2X\n",getGPIOFunction(GPIO2));
}


